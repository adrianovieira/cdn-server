FROM node:boron

# Create vendors lib directory
ENV CDN_ROOT_DIR=/usr/src/cdn-server
RUN mkdir -p $CDN_ROOT_DIR
WORKDIR $CDN_ROOT_DIR

# Install vendors dependencies
COPY package.json $CDN_ROOT_DIR
RUN npm install

ADD server.js $CDN_ROOT_DIR

EXPOSE 8080
CMD [ "npm", "start" ]
